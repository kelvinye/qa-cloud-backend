#!/usr/bin python3
# @File    : cache.py
# @Time    : 2023-04-23 17:28:53
# @Author  : Kelvin.Ye


# 接口描述字典（用于记录接口日志）
API_DOC_STORAGER = {}


# 运行中的pymeter的缓存，用于中断执行
executing_pymeters = {}


# 加密因子缓存
encryption_factors = {}
