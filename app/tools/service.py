#!/usr/bin/ python3
# @File    : service.py
# @Time    : 2020/1/14 10:49
# @Author  : Kelvin.Ye
from functools import wraps

from flask import g
from flask import request
from loguru import logger

from app.extension import db
from app.extension import socketio
from app.signals import openapi_log_signal
from app.signals import restapi_log_signal
from app.tools.exceptions import ServiceError
from app.tools.exceptions import ServiceStatus
from app.tools.request import RequestDTO
from app.tools.response import ResponseDTO
from app.tools.response import http_response
from app.utils.time_util import timestamp_as_ms


def http_service(func):
    """HTTP Service层装饰器，主要用于记录日志、耗时、捕获异常和事务控制"""

    @wraps(func)
    def wrapper(*args, **kwargs):
        # 记录开始时间
        starttime = timestamp_as_ms()
        # 获取请求对象
        req: RequestDTO = args[0] if args else None
        if req is None:
            req = kwargs.get('req', RequestDTO())
        if len(req) == 0:
            req.__attrs__ = request.json if request.is_json else request.values.to_dict()
        # 请求路径
        uri = f'{request.method} {request.path}'
        # 更新logger的record，使其指向被装饰的函数
        wlogger = logger.patch(lambda r: r.update(module=func.__module__, function=func.__name__))
        # 事务标识
        transaction = request.method != 'GET'
        # 注入traceid
        with logger.contextualize(traceid=g.trace_id):
            # 输出http请求日志
            req_headers = {name: value for name, value in request.headers.items() if name.lower() != 'access-token'}
            wlogger.info(f'uri:[ {uri} ] header:[ {req_headers} ] request:[ {req} ]')
            res = None
            try:
                # 判断request参数解析是否有异常
                if req.__error__ is not None:
                    res = ResponseDTO(msg=req.__error__, code=ServiceStatus.CODE_600.CODE)
                else:
                    # 调用service
                    result = func(*args, **kwargs)
                    res = ResponseDTO(result)
                    # 提交db事务
                    if transaction:
                        db.session.commit()
            except ServiceError as err:
                # 捕获service层的业务异常
                # 数据库回滚
                if transaction:
                    wlogger.info(f'uri:[ {uri} ] 数据回滚')
                    db.session.rollback()
                # 创建失败响应
                res = ResponseDTO(msg=err.message, code=err.code)
            except Exception:
                # 数据库回滚
                if transaction:
                    wlogger.error(f'uri:[ {uri} ] 数据回滚')
                    db.session.rollback()
                wlogger.exception(f'uri:[ {uri} ]')
                # 创建失败响应
                res = ResponseDTO(msg=ServiceStatus.CODE_500.MSG, code=ServiceStatus.CODE_500.CODE)
            finally:
                # 包装http响应
                http_res = http_response(res)
                # 记录接口耗时
                elapsed_time = timestamp_as_ms() - starttime
                # 记录请求日志
                if getattr(g, 'external_invoke', False):
                    # openapi日志
                    openapi_log_signal.send(
                        method=request.method,
                        uri=request.path,
                        request=req.__attrs__,
                        response=res.__dict__,
                        success=(res.code == 200),
                        elapsed=elapsed_time
                    )
                else:
                    # restapi日志
                    restapi_log_signal.send(
                        method=request.method,
                        uri=request.path,
                        request=req.__attrs__,
                        response=res.__dict__,
                        success=(res.code == 200),
                        elapsed=elapsed_time
                    )
                # 输出http响应日志
                wlogger.info(
                    f'uri:[ {uri} ] header:[ {dict(http_res.headers)}] response:[ {res} ] elapsed:[ {elapsed_time}ms ]'
                )
                return http_res

    return wrapper


def socket_service(func):
    """Socket Service层装饰器，主要用于记录日志、耗时、捕获异常和事务控制"""

    @wraps(func)
    def wrapper(*args, **kwargs):
        # 记录开始时间
        starttime = timestamp_as_ms()
        # 更新logger的record，使其指向被装饰的函数
        log = logger.patch(lambda r: r.update(module=func.__module__, function=func.__name__))
        # 获取接收数据
        event = request.event['message']
        data = args[0] if args else None
        sid = request.sid
        ns = request.namespace
        # 输出event日志
        log.info(f'namespace:[ {ns} ] socketid:[ {sid} ] event:[ {event} ] received:[ {data} ]')
        result = None
        try:
            # 调用service
            result = func(*args, **kwargs)
        except ServiceError as ex:
            log.info(f'namespace:[ {ns} ] socketid:[ {sid} ] event:[ {event} ] error:[ {str(ex)} ]')
            socketio.emit('service:error', str(ex))
        except Exception as ex:
            log.exception(f'namespace:[ {ns} ] socketid:[ {sid} ] event:[ {event} ]')
            socketio.emit('exception', str(ex))
        finally:
            # 记录接口耗时
            elapsed_time = timestamp_as_ms() - starttime
            # 输出event日志
            log.info(f'namespace:[ {ns} ] socketid:[ {sid} ] event:[ {event} ] elapsed:[ {elapsed_time}ms ]')
            return result

    return wrapper
