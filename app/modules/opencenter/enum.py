#!/usr/bin/ python3
# @File    : enum.py
# @Time    : 2023-04-17 17:26:00
# @Author  : Kelvin.Ye
from enum import Enum


class AppState(Enum):

    # 启用
    ENABLE = 'ENABLE'

    # 禁用
    DISABLE = 'DISABLE'
