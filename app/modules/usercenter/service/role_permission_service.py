#!/usr/bin/ python3
# @File    : role_permission_service.py
# @Time    : 2020/7/3 15:15
# @Author  : Kelvin.Ye
from app.database import db_query
from app.modules.usercenter.dao import role_dao
from app.modules.usercenter.dao import role_permission_dao
from app.modules.usercenter.model import TModule
from app.modules.usercenter.model import TObject
from app.modules.usercenter.model import TPermission
from app.modules.usercenter.model import TRolePermission
from app.tools.service import http_service
from app.tools.validator import check_exists
from app.utils.sqlalchemy_util import QueryCondition


@http_service
def query_role_permissions(req):
    conds = QueryCondition(TModule, TObject, TPermission, TRolePermission)
    conds.equal(TRolePermission.ROLE_NO, req.roleNo)
    conds.equal(TRolePermission.PERMISSION_NO, TPermission.PERMISSION_NO)
    conds.equal(TPermission.MODULE_NO, TModule.MODULE_NO)
    conds.equal(TPermission.OBJECT_NO, TObject.OBJECT_NO)

    resutls = (
        db_query(
            TModule.MODULE_CODE,
            TObject.OBJECT_CODE,
            TPermission.PERMISSION_NO,
            TPermission.PERMISSION_NAME
        )
        .filter(*conds)
        .order_by(TModule.MODULE_CODE.asc(), TObject.OBJECT_CODE.asc())
        .all()
    )

    return [
        {
            'permissionNo': resutl.PERMISSION_NO,
            'permissionName': resutl.PERMISSION_NAME
        }
        for resutl in resutls
    ]


@http_service
def set_role_permissions(req):
    # 查询角色
    role = role_dao.select_by_no(req.roleNo)
    check_exists(role, error='角色不存在')

    for permission_no in req.permissions:
        # 查询角色权限
        role_permission = role_permission_dao.select_by_role_and_permission(req.roleNo, permission_no)
        # 新增角色权限
        if not role_permission:
            TRolePermission.insert(ROLE_NO=req.roleNo, PERMISSION_NO=permission_no)

    # 删除不在请求中的角色权限
    role_permission_dao.delete_all_by_role_and_notin_permission(req.roleNo, req.permissions)
