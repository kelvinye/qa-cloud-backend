#!/usr/bin/ python3
# @File    : role_permission_controller.py
# @Time    : 2020/7/3 15:15
# @Author  : Kelvin.Ye
from app.modules.usercenter.controller import blueprint
from app.modules.usercenter.service import role_permission_service as service
from app.tools.parser import Argument
from app.tools.parser import JsonParser
from app.tools.require import require_login
from app.tools.require import require_permission


@blueprint.get('/role/permissions')
@require_login
@require_permission
def query_role_permissions(CODE='QUERY_ROLE'):
    """查询角色全部权限"""
    req = JsonParser(
        Argument('roleNo', required=True, nullable=False, help='角色编号不能为空')
    ).parse()
    return service.query_role_permissions(req)


@blueprint.post('/role/permissions')
@require_login
@require_permission
def set_role_permissions(CODE='MODIFY_ROLE'):
    """设置角色权限"""
    req = JsonParser(
        Argument('roleNo', required=True, nullable=False, help='角色编号不能为空'),
        Argument('permissions', type=list)
    ).parse()
    return service.set_role_permissions(req)
