#!/usr/bin/ python3
# @File    : workspace_restriction_dao.py
# @Time    : 2022/4/22 15:50
# @Author  : Kelvin.Ye
from app.modules.system.model import TWorkspaceRestriction


def select_by_workspace_and_permission(workspace_no, permission_no) -> TWorkspaceRestriction:
    return TWorkspaceRestriction.filter_by(WORKSPACE_NO=workspace_no, PERMISSION_NO=permission_no).first()


def select_all_by_workspace(workspace_no) -> list[TWorkspaceRestriction]:
    return TWorkspaceRestriction.filter_by(WORKSPACE_NO=workspace_no).all()


def delete_all_by_workspace_and_notin_permission(workspace_no, *permissions):
    TWorkspaceRestriction.deletes(
        TWorkspaceRestriction.WORKSPACE_NO == workspace_no,
        TWorkspaceRestriction.PERMISSION_NO.notin_(*permissions)
    )
