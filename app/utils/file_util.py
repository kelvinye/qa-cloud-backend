#!/usr/bin/ python3
# @File    : file_util.py
# @Time    : 2019/11/7 15:55
# @Author  : Kelvin.Ye
import os


def is_exists(path):
    return os.path.exists(path)
