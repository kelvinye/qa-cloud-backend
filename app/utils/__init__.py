#!/usr/bin/ python3
# @File    : __init__.py
# @Time    : 2019/11/7 9:50
# @Author  : Kelvin.Ye


"""
utils

通用的且与项目业务无关的类或函数，可供其他项目使用。
"""  # noqa: D212
